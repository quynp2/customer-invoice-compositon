
import com.devcamp.javabasic.s10.Customer;
import com.devcamp.javabasic.s10.Invoice;

public class App {
    public static void main(String[] args) throws Exception {
        Customer customer1 = new Customer();
        Customer customer2 = new Customer(002, "Tran Ngoc Thao", 20);
        System.out.println(customer1);
        System.out.println(customer1);

        Invoice invoice1 = new Invoice();
        Invoice invoice2 = new Invoice(007, customer1, 1000000);

        Invoice invoice3 = new Invoice(004, customer2, 30000000);

        System.out.println(invoice1);
        System.out.println(invoice2);
        System.out.println(invoice3);
        System.out.println(invoice2.getAmountAfterDiscount());
        System.out.println(invoice3.getAmountAfterDiscount());

    }
}
