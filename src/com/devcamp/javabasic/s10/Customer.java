package com.devcamp.javabasic.s10;

public class Customer {
    private int id;
    private String name;
    private int discount;

    public Customer() {
        this.id = 001;
        this.name = " Ngo Thi Hoang Nghia";
        this.discount = 30;

    }

    public Customer(int id, String name, int discount) {
        this.id = id;
        this.name = name;
        this.discount = discount;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    @Override
    public String toString() {
        return "Name: " + name + "Id: " + (id) + "Discount: " + (discount + "%");
    }

}
